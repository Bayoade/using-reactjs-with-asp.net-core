﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReactCore.Core.Models
{
    public class Element
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
