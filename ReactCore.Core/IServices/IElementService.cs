﻿using ReactCore.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ReactCore.Core.IServices
{
    public interface IElementService
    {
        Task<IList<Element>> GetAllElements();

        Task CreateElement(Element element);

        Task RemoveElement(Guid Element);

        Task UpdateElement(Element model);
    }
}
