﻿using ReactCore.Core.IServices;
using ReactCore.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactCore.Service.Services
{
    public class ElementService : IElementService
    {
        public async Task CreateElement(Element element)
        {
            var elements = await GetAllElements();

            elements.Add(element);
        }

        public Task<IList<Element>> GetAllElements()
        {
            return Task.FromResult<IList<Element>>( new List<Element>
            {
                new Element
                {
                    Id = Guid.Parse("d225d83c-2bb4-4a04-b9af-ecaafa1ad4af"),
                    Name = "Argon",
                    Description = "Part of the Element in Chemistry."
                }
            });
        }

        public async Task RemoveElement(Guid elementId)
        {
            var elements = await GetAllElements();

            var item = elements.FirstOrDefault(x => x.Id == elementId);
             elements.Remove(item);
        }

        public async Task UpdateElement(Element model)
        {
            var elements = await GetAllElements();
            var item = elements.FirstOrDefault(x => x.Id == model.Id);

            item.Name = model.Name;
            item.Description = model.Description;
        }
    }
}
