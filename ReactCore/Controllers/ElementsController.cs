﻿using Microsoft.AspNetCore.Mvc;
using ReactCore.Core.IServices;
using ReactCore.Core.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ReactCore.Controllers
{
    [Route("api/Elements")]
    [ApiController]
    public class ElementsController : ControllerBase
    {
        private readonly IElementService _elementService;

        public ElementsController(IElementService elementService)
        {
            _elementService = elementService;
        }

        [HttpGet]
        public async Task<IEnumerable<Element>> GetAllElements()
        {
            return await _elementService.GetAllElements();
        }

        [HttpPost]
        public async Task CreateElement(Element model)
        {
            await _elementService.CreateElement(model);
        }

        [HttpDelete]
        [Route("elementId")]
        public async Task Delete(Guid elementId)
        {
            await _elementService.RemoveElement(elementId);
        }

        [HttpPut]
        public async Task Update(Element model)
        {
            await _elementService.UpdateElement(model);
        }
    }
}